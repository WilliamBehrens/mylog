#include <iostream>
#include "mylog.hpp"

int main() { 
    std::cout << "Using log file test.log... " << std::endl;
    mylog::newLogFile("test.log");
   
    std::cout << "Testing mylog::info on log level 1... " << std::endl;
    mylog::info("Info is working!");
    
    std::cout << "Testing mylog::note on log level 1..." << std::endl;
	mylog::note("Note is working!");
     
    std::cout << "Changing log level to 3... " << std::endl;
    mylog::changeLogLevel(3);

    std::cout << "Testing mylog::info on log level 3... " << std::endl;
    mylog::info("You shouldn't see this message.");
    
    std::cout << "Testing mylog::note on log level 3... " << std::endl;
	mylog::note("You shouldn't see this message.");
    
    std::cout << "Changing log level back to 1... " << std::endl;
    mylog::changeLogLevel(1);
   
    std::cout << "Testing mylog::warning... " << std::endl;
    mylog::warning("Warning works!");

//////////////////////////////////////////////////////////////////////////

    std::cout << "Changing log file to test2.log... " << std::endl;
    mylog::newLogFile("test2.log");
	
    std::cout << "Testing mylog::info on log level 1... " << std::endl;
    mylog::info("Info is working!");
    
    std::cout << "Testing mylog::note on log level 1..." << std::endl;
	mylog::note("Note is working!");
     
    std::cout << "Changing log level to 4(errors only)... " << std::endl;
    mylog::changeLogLevel(4);

    std::cout << "Testing mylog::info on log level 3... " << std::endl;
    mylog::info("You shouldn't see this message.");
    
    std::cout << "Testing mylog::note on log level 3... " << std::endl;
	mylog::note("You shouldn't see this message.");
    
    std::cout << "Changing log level back to 1... " << std::endl;
    mylog::changeLogLevel(1);
   
    std::cout << "Testing mylog::warning... " << std::endl;
    mylog::warning("Warning works!");

////////////////////////////////////////////////////////////////////////////

    std::cout << "Removing test2.log, please check that the last test worked then press any key... " << std::endl;
    std::cin.get();
    mylog::removeLogFile();
	
    std::cout << "Changing log file to test.log... " << std::endl;
    mylog::newLogFile("test.log");

    std::cout << "Printing log level... " << std::endl;
    std::cout << mylog::getLogLevel() << std::endl;
    
    
    std::cout << "Testing mylog::error, this will close the program, press any key when ready... " << std::endl;
    std::cin.get();
	mylog::error("mylog::error works!", 69);
    std::cout << "WARNING: mylog::error did not close properly!" << std::endl;
    
    return 1;
}
