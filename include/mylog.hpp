#pragma once

#include <string>
#include <fstream>

// A singleton class to handle logging fuctionality
class mylog { 	
	public:
	mylog(const mylog&) = delete;
	static void info(std::string);
	static void note(std::string);
	static void warning(std::string);
	static void error(std::string, int errorCode);
	static void newLogFile(std::string fileName);
	static void changeLogLevel(std::uint_fast8_t newLevel);
	static bool removeLogFile();
	static int getLogLevel();

	static mylog& get();

	protected:
	std::uint_fast8_t logLevel = 1; // 1 = all, 2 = warnings + errors + info, 3 = errors + info, 4 = errors
	void writeToFile(std::string message);
	bool testFile(); // Test if the log file exist and tries to create it if it doesn't

	void infoImpl(std::string);
	void noteImpl(std::string);
	void warningImpl(std::string);
	void errorImpl(std::string, int errorCode);
	void newLogFileImpl(std::string fileName);
	void changeLogLevelImpl(std::uint_fast8_t newLevel);
	bool removeLogFileImpl();
	int getLogLevelImpl();



	private:
	mylog();
	std::fstream _logFile;
	bool _logExist = false;
	std::string _logName = "out.log";
	
};
